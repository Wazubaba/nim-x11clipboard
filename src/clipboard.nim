{.passL: "-lX11".}
{.compile: "clipboard.c".}

type
  bufferType = enum
    clipboard = 0,
    primary = 1
  
  formatType = enum
    utf8 = 0,
    ascii = 1

proc c_getClipboard(bufferType, fmtType: cint): cstring {.importc: "getClipboard".}
proc c_cleanup(str: cstring) {.importc: "cleanup".}

proc getClipboard*(bt: bufferType = clipboard, ft: formatType = utf8): string =
  let btval = case bt:
    of clipboard: cint(0)
    of primary: cint(1)
  
  let ftval = case ft:
    of utf8: cint(0)
    of ascii: cint(1)

  let output = c_getClipboard(btval, ftval)
  result = $output
  c_cleanup(output)

echo getClipboard()

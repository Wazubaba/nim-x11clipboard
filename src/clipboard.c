#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <limits.h>
#include <X11/Xlib.h>


/* Fuck learning x11 for something that should be fucking simple for now.
 * Shamelessly copied from
 * https://stackoverflow.com/a/44992938 <- this guy's second example, with slight
 * edits so I can make it into a simple function call. Fuck is linux retarded the
 * second you try to do anything outside of a terminal...
*/

/* Holy fuck I am having a bad day. Here's a fucking strdup implementation courtesy
	https://stackoverflow.com/q/32944390
	WHY DO I NEED TO DEFINE RANDOM SHIT FOR SOMETHING THIS SIMPLE!?
*/
char *strdup(const char *s) {
	size_t size = strlen(s) + 1;
	char *p = malloc(size);
	if (p) {
		memcpy(p, s, size);
	}
	return p;
}

enum BUFFER
{
	BUFFER_CLIPBOARD = 0,
	BUFFER_PRIMARY = 1
};

enum FORMAT
{
	FORMAT_UTF8 = 0,
	FORMAT_ASCII = 1
};


char* getClipboard(const int bufferType, const int fmtType)
{
  Display *display = XOpenDisplay(NULL);
  unsigned long color = BlackPixel(display, DefaultScreen(display));
  Window window = XCreateSimpleWindow(display, DefaultRootWindow(display), 0,0, 1,1, 0, color, color);
	char bufname[32];
	char fmtname[32];
	switch(bufferType)
	{
		case BUFFER_CLIPBOARD:
			strcpy(bufname, "CLIPBOARD");
			break;
		case BUFFER_PRIMARY:
			strcpy(bufname, "PRIMARY");
			break;
	};

	switch(fmtType)
	{
		case FORMAT_UTF8:
			strcpy(fmtname, "UTF8_STRING");
			break;
		case FORMAT_ASCII:
			strcpy(fmtname, "STRING");
			break;
	}


  char *result;
	char* returnval = NULL;
  unsigned long ressize, restail;
  int resbits;
  Atom bufid = XInternAtom(display, bufname, False),
       fmtid = XInternAtom(display, fmtname, False),
       propid = XInternAtom(display, "XSEL_DATA", False),
       incrid = XInternAtom(display, "INCR", False);
  XEvent event;

  XSelectInput (display, window, PropertyChangeMask);
  XConvertSelection(display, bufid, fmtid, propid, window, CurrentTime);
  do {
    XNextEvent(display, &event);
  } while (event.type != SelectionNotify || event.xselection.selection != bufid);

  if (event.xselection.property)
  {
    XGetWindowProperty(display, window, propid, 0, LONG_MAX/4, True, AnyPropertyType,
      &fmtid, &resbits, &ressize, &restail, (unsigned char**)&result);
    if (fmtid != incrid)
      returnval = strdup(result);
    XFree(result);

    if (fmtid == incrid)
      do {
        do {
          XNextEvent(display, &event);
        } while (event.type != PropertyNotify || event.xproperty.atom != propid || event.xproperty.state != PropertyNewValue);

        XGetWindowProperty(display, window, propid, 0, LONG_MAX/4, True, AnyPropertyType,
          &fmtid, &resbits, &ressize, &restail, (unsigned char**)&result);
				returnval = strdup(result);
        XFree(result);
      } while (ressize > 0);
  }

  XDestroyWindow(display, window);
  XCloseDisplay(display);
	return returnval;
}

/* This is for nim's sake */
void cleanup(char* string)
{
	free(string);
}

#ifdef _DEBUG_MAIN
int main(void)
{
	char* output = getClipboard(BUFFER_CLIPBOARD, FORMAT_ASCII);
	printf("%s\n", output);
	free(output);
	output = getClipboard(BUFFER_PRIMARY, FORMAT_UTF8);
	printf("\n%s\n", output);
	free(output);
  return 0;
}
#endif
